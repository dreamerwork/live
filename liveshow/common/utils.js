export default {
	/**
	 * 表单验证提示 无按钮
	 * @param {title} 提示文本信息
	 * @param {duration} 延迟时间
	 * icon设置为none后，一行最多容纳11字
	 * h5多行，小程序两行，也就是22字
	 */
	tip(title, duration = 1500, icon = 'none') {
		uni.showToast({
			title: title,
			duration: duration,
			icon: icon // success，loading
		});
	},
	/**
	 * @param {mobile} 手机号码
	 */
	mobileReg(mobile) {
		const mobileReg = /^1[0-9]{10,10}$/;
		return mobileReg.test(mobile)
	},
	/**
	 * @param 本地储存 uni.setStorageSync(KEY,DATA)  同步
	 */
	setStorageSync(keyName,data) {
		 uni.setStorageSync(keyName, data);
	},
	/**
	 * @param 本地储存 uni.getStorageSync(KEY,DATA)  同步
	 */
	getStorageSync(keyName) {
		 return uni.getStorageSync(keyName);
	},
	/**
	 * @param 本地储存 uni.removeStorageSync(KEY,DATA)  异步
	 */
	removeStorage(keyName) {
		return new Promise(function(resolve,reject) {
			uni.removeStorage({
			    key: keyName,
			    success: function (res) {
					if(res.errMsg == 'removeStorage:ok') {
						resolve(true)
					}
			    }
			});
			uni.removeStorageSync(keyName);
		})
	},
	removeStorageSync(keyName) {
		// return new Promise(function(resolve,reject) {
		// 	uni.removeStorage({
		// 	    key: 'storage_key',
		// 	    success: function (res) {
		// 	        console.log('success');
		// 	    }
		// 	});
		// 	uni.removeStorageSync(keyName);
		// 	resolve(true)
		// })
	}
	
}
